﻿#include <iostream>
#include <math.h>
#include <string>

using namespace std;


class Vector
{

public:
    Vector(): x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}

    double length()
    {
        double len = sqrt(x*x + y*y + z*z);
        return len;
    }

    void show()
    {
        cout << "Vector "<< "{" << x << "; " << y << "; " << z << "}";
    }

private:
    double x, y, z;
    
};

class MyClass
{
public:
    int qualification;

    MyClass() : name(), surname(), qualification()
    {}
    MyClass(string _name, string _surname, int _qualification) : name(_name), surname(_surname), qualification(_qualification)
    {}
    ~MyClass()
    {
        
    }
   

    void info()
    {
        cout << name << surname << ", qualification: "<<qualification;
    }
    
   
private:
    
    string name;
    string surname;
    
    
}; 




int main()
{
    setlocale(LC_ALL, "Russian");
    double i(1.3), j(2), k(32);
    Vector v(i, j, k);
    cout << "2-nd part of the task: \n" << endl;
    v.show();
    cout << "\nVector length: " << v.length() << "\n\n";


    cout << "1-st part of the task: \n" << endl;
   
   
    MyClass workers[] = { MyClass("S. ","Shirokov", 1), MyClass("V. ","Bolotin", 2),
                          MyClass("E. ","Neprin", 3), MyClass("D. ","Treshev", 0) };

    int length = sizeof(workers)/ sizeof(workers[0]);
    
   //   Сортировка массива

    for (int i = 0; i < length - 1; i++)
    {
        int s = i;
        for (int j = i + 1; j < length; j++)
        {
            if (workers[j].qualification < workers[s].qualification)
            {
                s = j;
            }
        }

        swap(workers[i], workers[s]);
    }
    
    


    for (int i = 0; i < length; i++)
    {
        workers[i].info();
        cout << '\n';
    }

   
    cout << "Наберайтесь опыта и возвращайтесь к нам снова, " ;
    workers[0].info();
    workers[0].~MyClass();
    cout << endl;

    for (int i = 0; i < length; i++)
    {
        workers[i].info();
        cout << '\n';
    }

}